package itau.com.internationaltranf;

import java.util.ArrayList;
import java.util.List;

public class GerenciadorCliente {
	
	List<Cliente> clientes = new ArrayList<Cliente>();
	
	public GerenciadorCliente() {
		
		GerenciadorArquivo gerArq = new GerenciadorArquivo();
		List<String> arqContas = gerArq.leArquivoContas();
	
		for(String linha: arqContas) {
			String[] partes = linha.split(",");	
			String nome = partes[0];	
			String username = partes[1];
			String senha = partes[2];
			double saldo = Double.parseDouble(partes[3]);
		    this.clientes.add(new Cliente(nome, username, senha, saldo));
			
		}	
	}
	public Cliente getCliente(int n) {
	
			return this.clientes.get(n);
	}

}

