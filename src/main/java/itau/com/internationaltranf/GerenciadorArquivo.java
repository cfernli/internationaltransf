package itau.com.internationaltranf;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class GerenciadorArquivo {
	
	public List<String> leArquivoContas() {
		
		Path path = Paths.get("./src/accounts.csv");
		List<String> listaContas;
		try {
			listaContas = Files.readAllLines(path);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Arquivo de contas não encontrado.");
			return null;
		}
		listaContas.remove(0);
		return listaContas;
	}
	
    public List<String> leArquivoTransacoes() {
		
		Path path = Paths.get("./src/withdrawals.csv");
		List<String> listaTransacoes;
		try {
			listaTransacoes = Files.readAllLines(path);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Arquivo de transferências não encontrado.");
			return null;
		}
		listaTransacoes.remove(0);
		return listaTransacoes;
	}
    
    public void gravaArquivoContas(StringBuilder lista) {
    	
    	Path outputPath = Paths.get("./src/accounts2.csv");
	
    	try {
			Files.write(outputPath, lista.toString().getBytes());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}  

}

