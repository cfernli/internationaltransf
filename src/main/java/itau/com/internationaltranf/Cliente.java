package itau.com.internationaltranf;

public class Cliente {
	
	String nome;
	String username;
	String senha;
	double saldo;
	
	public Cliente(String nome, String username, String senha, double saldo) {
		this.nome = nome;
		this.username = username;
		this.senha = senha;
		this.saldo = saldo;
	}
	
	public String getNome() {
		return this.nome;
	}
	
	public String getUsername() {
		return this.username;
	}
	
	public String getSenha() {
		return this.senha;
	}
	
	public double getSaldo() {
		return this.saldo;
	}

}
	
	