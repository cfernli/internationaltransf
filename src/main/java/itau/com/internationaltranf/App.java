package itau.com.internationaltranf;

import java.util.List;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;

public class App {
    public static void main( String[] args ) {
    	
    	GerenciadorCliente gerCli = new GerenciadorCliente();
    	GerenciadorArquivo gerArq = new GerenciadorArquivo();
    	List<String> arqTransacoes = gerArq.leArquivoTransacoes();
		StringBuilder listaSaida = new StringBuilder();
		Cliente cliente = null;
		
		for(String linha: arqTransacoes) {
			String[] partes = linha.split(",");	
			String username = partes[0];
		    double valTransf = Double.parseDouble(partes[1]);
		    String moeda = partes[2];
		   
		    for(int i=0; i < gerCli.clientes.size();i++) {
		    	cliente = gerCli.getCliente(i);
		    	if (username.equals(cliente.username)){
		    		double saldo = cliente.saldo;
		    		double cotacao = buscaCotacao(moeda);    	
		    		saldo = (saldo - (valTransf/cotacao));  		
		    		String arqSaida = String.format("%s, %s, %s, %s\n", cliente.nome, cliente.username, cliente.senha, saldo); 
					listaSaida.append(arqSaida);
		    	} else {
		    		String arqSaida = String.format("%s, %s, %s, %s\n", cliente.nome, cliente.username, cliente.senha, cliente.saldo); 
					listaSaida.append(arqSaida);
		    	}
		    }
		}
		gerArq.gravaArquivoContas(listaSaida);
		System.out.println("Processamento concluído com sucesso.");
    }
    
    public static double buscaCotacao(String moeda) {
    	double cotacao = 0.00;
    	Gson gson = new Gson();
    	String response = HttpRequest.get("http://data.fixer.io/api/latest?access_key=578ac533a860ac0f3d508e3dbad0ef57").body();
    	Fixer fixer = gson.fromJson(response, Fixer.class);
    	cotacao = fixer.rates.get(moeda);
    	return cotacao;	
    }
}

